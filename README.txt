
Drupal shareomatic README
---------------------------------
This Drupal Shareomatic module is heavily based on the DiggThis  module by 
Ramiro G�mez (http://www.ramiro.org)

Requirements
------------
This version of the module requires Drupal 6.X.

Overview:
--------
This module adds a Shareomatic this button to your nodes. 

Installation and configuration:
------------------------------
Simply extract the download package in your modules directory and 
then enable the module in 'admin/build/modules/'.
